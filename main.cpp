#include <iostream>
#include "myvector.h"

template<typename T>
myvector::vector<T> doubleValue(const myvector::vector<T>& v);

int main() {
	const size_t LEN = 5;
	double data[LEN] = { 1.0, 2.0, 3.0, 4.0, 5.0 };

	cout << "create v1:" << endl;
	myvector::vector<double>v1(data, LEN);
	cout << "create v2:" << endl;
	myvector::vector<double>v2(v1);
	cout << "create v3:" << endl;
	myvector::vector<double>v3;
	cout << "assigning v2 to v3" << endl;
	v3 = v2;

	cout << "create v4" << endl;
	myvector::vector<double> v4;
	cout << "double v1 to v4" << endl;
	v4 = doubleValue<double>(v1);

	cout << "disp table" << endl;
	v1.disp();
	v2.disp();
	v3.disp();
	v4.disp();

	return 0;
}

template<typename T>
 myvector::vector<T> doubleValue(const myvector::vector<T>& v) {
	size_t len = v.length();
	T* data = new T[len];

	for (size_t i = 0; i < len; i++) {
		data[i] = 2.0 * v[i];
	}
	myvector::vector<T> newv(data, len);
	
	return newv;
}