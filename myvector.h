#ifndef MYVECTOR_H_
#define MYVECTOR_H_


#include <iostream>
using std::cout;
using std::endl;

namespace myvector{
	template<typename Type>
	class vector {
	public:
		vector();
		vector(const Type* _data, size_t _len);
		vector(const vector& v);

		const vector& operator=(const vector& v);
		
		~vector();

		size_t length()const;
		Type operator[](size_t i)const;
		void disp()const;

	private:
		Type* data;
		size_t len;
	};
    
	template<typename Type>
	vector<Type>::vector(){
		data = nullptr;
		len = 0;
	}

	template<typename Type>
	vector<Type>::vector(const Type* _data, size_t _len){
		len = _len;
		data = new Type[len];

		for (size_t i = 0; i < len; ++i){
			data[i] = _data[i];
		}
	}

	template<typename Type>
	vector<Type>::vector(const vector<Type>& v){
		len = v.len;
		data = new Type[len];

		for (size_t i = 0; i < len; ++i){
			data[i] = v.data[i];
		}
#ifdef SHOW_CON
		cout << "copy constructor" << endl;
#endif 
	}

	template<typename Type>
	const vector<Type>& vector<Type>::operator=(const vector<Type>& v){
		len = v.len;
		data = new Type[len];

		for (size_t i = 0; i < len; ++i) {
			data[i] = v.data[i];
		}
#ifdef SHOW_CON
		cout << "operator assignment" << endl;
#endif 
		return *this;
	}

	template<typename Type>
	vector<Type>::~vector() {
		delete[] data;
	}

	template<typename Type>
	size_t vector<Type>::length()const {
		return len;
	}

	template<typename Type>
	Type vector<Type>::operator[](size_t i)const {
		return data[i];
	}

	template<typename Type>
	void vector<Type>::disp()const {
		cout << "length: " << len << endl;
		for (size_t i = 0; i < len; i++) {
			cout << data[i] << " ";
		}
		cout << endl;
	}
}

#endif